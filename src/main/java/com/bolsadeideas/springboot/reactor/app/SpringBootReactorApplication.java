package com.bolsadeideas.springboot.reactor.app;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bolsadeideas.springboot.reactor.app.models.Usuario;

import reactor.core.publisher.Flux;

@SpringBootApplication
public class SpringBootReactorApplication implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringBootReactorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		//Los flujos SON INMUTABLES, cada vez que llamemos a un operador se crea un nuevo flujo
		//Flux<String> nombres = Flux.just("Andres Guzmán","Pedro Fulano","María Fulana","Diego Sultano","Juan Mengano","Bruce Lee","Bruce Willis");
		
		List<String> nombresList = Arrays.asList("Andres Guzmán","Pedro Fulano","María Fulana","Diego Sultano","Juan Mengano","Bruce Lee","Bruce Willis");
		Flux<String> nombres = Flux.fromIterable(nombresList);
		
		Flux<Usuario> usuarios = nombres.map(nombre -> //Devuelve otra instancia de Flux con la aplicación del map
					new Usuario(nombre.split(" ")[0].toUpperCase(),nombre.split(" ")[1].toUpperCase())
				)
				.filter(usuario -> {
					return "bruce".equals(usuario.getNombre().toLowerCase());
				})
				.doOnNext(usuario -> {
					if(usuario == null) {
						throw new RuntimeException("Nombres no pueden ser vacíos");
					}
					System.out.println(usuario.getNombre().concat(" ").concat(usuario.getApellido()));
				})
				.map(usuario -> { //Devuelve otra instancia de Flux con la aplicación del map
					usuario.setNombre(usuario.getNombre().toLowerCase());
					return usuario;
				});
		
		//Cuando nos subscribimos estamos 'observando' los eventos que emite el flujo
		usuarios.subscribe(
				e -> log.info(e.toString()),
				error -> log.error(error.getMessage()),
				new Runnable() {
					public void run() {
						log.info("Ha finalizado la ejecución del observable flux");
					}
				}); //Si no nos suscribimos al flujo no va a ocurrir nada
	}
}